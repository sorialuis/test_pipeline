const AWS = require('aws-sdk')
const dynamobd = new AWS.DynamoDB.DocumentClient()


exports.saveHello = async (event, context) => {
    const body = JSON.parse(event.body)
    const item = {
        id: body.name,
        name: body.name,
        date: Date.now()
    }
    
    const savedItem = await saveItem(item)
    return {
        body: JSON.stringify(savedItem),
        statusCode: 200
    } 
}

exports.getHello = async(event, contex) => {
    console.log("hola")
    const name = event.queryStringParameters.name;
    try {
        const item = await getItem(name)
        console.log(item)
        if (item.date) {
            const d = new Date(item.date)
            return {
                statusCode: 200,
                body: `Hola ${item.name} creado el ${d.getDate()}/${d.getMonth()+1}/${d.getFullYear()}`
            }
        }
    } catch (e) {
        console.log(e)
        return {
            statusCode: 200,
            body: "No tenemos ese nombre"
        }
    }
} 

async function saveItem(item){
    const params = {
        TableName: process.env.HELLO_TABLE,
        Item: item
    }

    return dynamobd.put(params).promise().then(() => {
        return item
    })
}

async function getItem(name){

    const params = {
        Key: {
            id: name
        },
        TableName: process.env.HELLO_TABLE
    }
    console.log("params", params)
    return dynamobd.get(params).promise().then(result => {
        return result.Item
    })
}